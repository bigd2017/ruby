require ('sinatra')
require ('sinatra/contrib/all')
# require ('pry-byebug')
require_relative ('./models/rsp.rb')
also_reload('models/*')


get ("/play_game/:player1/:player2") do
  game = Game.new(params[:player1],
  params[:player2])
  @result = game.play_game()
  erb(:result)
end



get "/" do
  erb(:home)
end

get '/rules' do
  erb(:rules)
end
