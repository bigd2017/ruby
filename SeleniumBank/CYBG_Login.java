import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Parameters;

import javax.swing.JOptionPane;


public class CYBG_Login {
	// url to connect to main application screen
	final static String URL = "https://developer:############@dyb.dybapplications.co.uk/####/###/authentication?applyFor=IM136";
	final static String uWord = "Password@123";// 12 chars.
	final static String password = "test123";
	final static String numb = "1060371713";
	final static String amnt = "2";
	static WebDriver dr;

	//retrieve response code to validate connection is working properly
	public static void ConnTest() throws IOException {
		java.net.URL url = new java.net.URL(URL);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.connect();
		String code = connection.getResponseCode() + "\n" + connection.getResponseMessage();
		System.out.println("Response :\n" + code);
		
	}
	
	
	//validate the functionality of the appropriate web-links, buttons and 'payment' utility of CYB website
	public static void FrontEndTest() {
		// store data from LoginCYBG in arraylist & add values
		List<LoginCYBG> data = new ArrayList<LoginCYBG>();
		data.add(new LoginCYBG(uWord, password));

		System.setProperty("webdriver.chrome.driver", "D:/Chrome/chromedriver.exe");
		dr = new ChromeDriver();
		dr.get(URL);

		WebElement var = dr.findElement(By.xpath("//*[@id='CustomerNumber']"));
		var.sendKeys(numb);
		System.out.println("pass");

		// wait for screen to load properly
		dr.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		// now 'click' continue button to proceed
		dr.findElement(By.xpath("//*[(text()='Continue')]")).click();
		List<WebElement> ele = dr.findElements(By.xpath("//input[@type='password']"));
		System.out.println("\n");

		// Loop through each row
		for (WebElement user : ele) {
			// Get thepreceding label element
			WebElement username = user.findElement(By.xpath(".//preceding::label[1]"));

			// fetch 1st label text i.e.first character, split contents, parse
			// and store as int variable for input as character
			String str = username.getText().split(" ")[1];
			int i = Integer.parseInt(str);
			char value = uWord.charAt(i - 1); // i.e). 1st char:'p' for password (last char.)

			dr.findElement(By.xpath("//input[@id='partial-password-0']"));
			final String strPassword = Character.toString(value);
			user.sendKeys(strPassword); // insert char into password box
		}
		// now 'click' continue button to proceed
		dr.findElement(By.xpath("//*[(text()='Continue')]")).click();
		// input passphrase into security question widget
		dr.findElement(By.name("security-question-0")).sendKeys(password);
		// now 'click' continue button to proceed
		dr.findElement(By.xpath("//*[(text()='Continue')]")).click();
		// finally...locate the Current B 'Balance' text on-screen 
		// Retrieve the HTML for displaying on ide console...
		String text = dr.findElement(By.xpath("//*[@id='account 1']//following::*[@class='c-account__balance'][1]"))
				.getText();
		System.out.println("B Current: Balance \n" + text);
		System.out.println(" ");
	}
	
	
	//now that account is selected, choose the on-screen widgets eg) pot, to make the payment. Print-out whether TC is 'PASS'.
	public static void Task() {
		// now 'click' continue button to proceed
		dr.findElement(By.xpath("//a[@class='c-btn c-btn--secondary']")).click();// button[@id='toAccount']
		System.out.print("PASS: 1 \n");
		// now 'click' on 'From' button
		dr.findElement(By.xpath("//*[@class='add-button']")).click();
		System.out.print("PASS:2 \n");

		// now 'click' on 'B Instant Savings' button
		dr.findElement(By.xpath("//*[(text()='B Current')]")).click();
		System.out.print("PASS:3 \n");
		// now choose which 'pot to select
		dr.findElement(By.xpath("//button[@id='toAccount']")).click();
		System.out.print("PASS:4 \n");

		// select payee
		dr.findElement(By.xpath("//*[@class='account-card__reference']")).click();
		System.out.print("PASS:5 payee\n:");

		WebElement Box = dr.findElement(By.xpath("//input[@id='amtId']"));
		System.out.print("PASS:6 amnt\n:");
		//String input =  JOptionPane.showInputDialog(null,"Enter amount, please");
		//JOptionPane.showMessageDialog(null,input);
		Box.sendKeys(amnt);
		System.out.print("PASS:7 send amnt: ");

	}
	
	
	//print-out screen mesage to ide console as required 
	public static void Review() {
		dr.findElement(By.xpath("//*[(text()='Review')]")).click();
		System.out.print("PASS:8: review status \n");

		dr.findElement(By.xpath("//*[(text()='Make payment')]")).click();
		System.out.print("PASS:9: make payment\n");

		String pay = dr.findElement(By.xpath("//*[@class='c-media move-money__summary__message']")).getText();
		System.out.println("PASS : 10 : +" + "\n " + pay);
		
		//leave open for now...
		//dr.close();
	}
	
	
	//main method launch
	@Parameters
	public static void main(String[] args) throws IOException {

		ConnTest();
		FrontEndTest();
		Task();
		Review();
	}

}
