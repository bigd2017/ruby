import java.util.List;

// LoginData Object class
public class LoginCYBG {

	private String _username;
	private String _password;

	// Constructor to set properties/characteristics of object
	// username AKA customer username and password
	public LoginCYBG(String username, String password) {

		this.set_username(username);
		this.set_password(password);
	}

	public String get_username() {
		return _username;
	}

	private void set_username(String _username) {
		this._username = _username;
	}

	public String get_password() {
		return _password;

	}

	private void set_password(final String password) {
		this._password = password;
	}

}