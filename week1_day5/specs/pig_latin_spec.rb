require( 'minitest/autorun' )
require( 'minitest/rg' )
require_relative( '../my_functions.rb' )

class TestMyFunctions < MiniTest::Test

  def test_My_Functions
    result = piglatin("This is a test of PigLatin")
    assert_equal(result, "Histay siay aay esttay foay iglatinpay")
  end

end
