class Animal
  def initialize
    puts "creating new animal"
  end

  def set_name(new_name)
    @name= new_name
  end

  def get_name()
    @name
  end

  def name
    @name
  end

  def name=(new_name)
    if new_name.is_a?(Numeric)
      puts "cannot be a number!"
    else @name = new_name
    end
  end
end

cat = Animal.new

cat.set_name("titachaio")
puts cat.get_name
puts cat.name
cat.name = "littlesock"
puts cat.name

class Dog
  # attr_reader :name, :height, :weight
  # attr_writer :name, :height, :weight
  # Creates setter and getter methods (Use this One)
  attr_accessor :name, :height, :weight

  def bark
    return "bark"
  end

  rover = Dog.new
  rover.name = "fido"
  puts rover.name
  puts rover.bark

  # When you inherit from another class you get all its methods and variables
  # You can only inherit from one class

  class WestieDog < Dog
    # You can overwrite methods
    def bark
      return "Loud Bark"
    end
  end

  max = WestieDog.new

  max.name = "Max"

  printf "%s goes %s \n", max.name, max.bark()


end
