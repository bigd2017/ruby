require('sinatra')
require('sinatra/contrib/all')

get '/hi' do  #when  ruby sees 'hi' execte do block
  return  'Hello world!'
end

get  "/roll-die" do
  return "I rolled die number  #{rand(1..6)}"
end

get "/name/:first/:last" do
  i = 7
  g = 9
  result = i*g
  return "Hello #{params[:first]}  #{params[:last]} #{rand(result)}!"
end

#always put the more Specific routes above the more general
get "/friends/john" do
  return "hi john"
end

get  "/friends/:number" do
  friends = ["jerry", "david", "chen", "mark"]
  index = params[:number].to_i
  return friends[index]
end
