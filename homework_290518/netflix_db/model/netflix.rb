require('pg')
class Netflix

  attr_accessor :filmid, :customerid, :genre, :netflix_sub, :age
  attr_reader :id
  def initialize(options)
    @id = options['id'].to_i if options['id']
    @filmid = options['filmid']
    @customerid =options['customerid']
    @genre = options['genre']
    @netflix_sub = options['netflix_sub']
    @age =options['age'].to_i
  end

  #retrieve/extract data from db table
  def Netflix.all()
    db = PG.connect({
      dbname: 'netflix',
      host: 'localhost'
      })
      sql = "select * from netflix"
      db.prepare("all",sql)
      hunt = db.exec_prepared("all")
      db.close()
      return hunt.map {|hunt| Netflix.new(hunt)}
    end

    def Netflix.delete_all()
      db = PG.connect({
        dbname: 'netflix',
        host: 'localhost'
        })
        sql = "delete from netflix"
        db.prepare("delete_all", sql)
        db.exec_prepared("delete_all")
        db.close()
      end
      #connect db and save data cols/values in db table.
      def save()
        db = PG.connect({
          dbname: 'netflix',
          host: 'localhost'
          })
          sql = "INSERT INTO netflix (filmid,customerid,genre,netflix_sub, age)
          VALUES ($1, $2, $3, $4, $5) returning *"#return all
          values = [ @filmId, @customerId, @genre, @netflix_sub, @age]
          db.prepare("save", sql)
          @filmid = db.exec_prepared("save", values)[0]["filmid"]#return the id fro 1st element in array [id] &
          #and store in @id variable
          db.close()
        end

        def Netflix.find_by_genre(genre)
          db = PG.connect({
            dbname: 'netflix',
            host: 'localhost'
            })
            sql = "select * from netflix where genre = $1 "
            values = [species]
            db.prepare("find_by_genre", sql)
            found = db.exec_prepared("find_by_genre", values)
            db.close
            found_array = found.map{|species| netflix.new(genre)}
            return found_array
            return found
          end

          def delete ()
            db = PG.connect({
              dbname: 'netflix',
              host: 'localhost'
              })
              sql = "delete from netflix where id = $1"
              values = [@id]
              db.prepare("delete", sql)
              db.exec_prepared("delete",values)
              db.close()
            end

            def update ()
              db=PG.connect({
                dbname: 'netflix',
                host: 'localhost'
                })
                sql = "update netflix SET (filmid,customerid,genre,netflix_sub, age)
                = ($1,$2,$3,$4) where id = $5"
                values = [@filmId, @customerId, @genre, @netflix_sub, @age, @id]
                db.prepare("update",sql)
                db.exec_prepared("update",values)
                db.close()
              end
            end
