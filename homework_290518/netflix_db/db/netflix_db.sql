DROP TABLE IF EXISTS netflix;

create table netflix(
  id SERIAL4 primary key,
  customerid varchar(255),
  filmid  varchar(255),
  genre  varchar(255),
  netflix_sub varchar(255),
  age INT2
);
