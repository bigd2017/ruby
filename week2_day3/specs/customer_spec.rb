require_relative("../drink")
require_relative("../pub")
require_relative("../customer")
require("minitest/rg")
require("minitest/autorun")


class CustomerTest < MiniTest::Test


  def setup
    @customer = Customer.new("Frodo", 10.0)
  end

  def test_customer_has_name
    assert_equal("Frodo", @customer.name())
  end

  def test_customer_has_money
    assert_equal(10.0, @customer.wallet())
  end

end
