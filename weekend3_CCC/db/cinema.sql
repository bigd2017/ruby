DROP VIEW IF EXISTS cust_film_choice_count;
DROP TABLE IF EXISTS tickets;
DROP TABLE IF EXISTS screenings;
DROP TABLE IF EXISTS films;
DROP TABLE IF EXISTS customers;



CREATE TABLE customers (
  id SERIAL4 PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  funds INT4
);

CREATE TABLE films (
  id SERIAL4 PRIMARY KEY,
  title VARCHAR(255) NOT NULL,
  price INT4 NOT NULL
);

CREATE TABLE screenings(
  id SERIAL4 PRIMARY KEY,
  showing_time INT4
);

CREATE TABLE tickets (
  id SERIAL4 PRIMARY KEY,
  customer_id INT4 REFERENCES customers(id) ON DELETE CASCADE,
  film_id INT4 REFERENCES films(id) ON DELETE CASCADE,
  screen_id INT4 REFERENCES screenings(id) ON DELETE CASCADE
);

CREATE VIEW cust_film_choice_count AS (
  select distinct c.name, f.title, count(*) from customers c inner join
  tickets t on (c.id = t.customer_id)
  inner join films f on (t.film_id = f.id)
  where title = 'Die Hard' group by name,title order by count(*) desc
);
