require_relative("../db/sql_runner")
require_relative("customer")
require_relative("ticket")
require_relative("screening")

class Film
  attr_reader :id
  attr_accessor :title, :price
  def initialize( options )
    @id = options['id'].to_i if options['id']
    @title = options['title']
    @price = options['price']

  end

  #CREATE
  def save()
    sql = "INSERT INTO films
    (  title, price)
    VALUES
    (  $1,$2)
    RETURNING id"
    values = [@title, @price]
    film = SqlRunner.run( sql, values ).first
    @id = film['id'].to_i
  end

  #UPDATE function
  def update()
    sql = "UPDATE films SET (title, price) = ($1, $2 WHERE id = $3)"
    values = [@title, @price, @id]
    SqlRunner.run(sql, values)
  end

  # display all the films a customer has booked to see
  def customer()
    sql = "select distinct c.id, c.name, f.title from customers c inner join
    tickets t on (c.id = t.customer_id)
    inner join films f on (t.film_id = f.id)
    WHERE film_id = $1 order by name, title desc"
    values = [@id]
    cust_data = SqlRunner.run(sql, values)
    return Customer.map_items(cust_data)
  end

  # display all the films a customer has booked to see only 1 film
  def count_films()
    sql = "select distinct count(c.id), c.name, f.title from customers  c inner join tickets t
    on (c.id = t.customer_id) inner join films f on (t.film_id = f.id)
    group by c.id, c.name, f.title, f.price having count(*) =1 order by name"
    values = [@id]
    cust_count = SqlRunner.run(sql, values)
    return Customer.map_items(cust_count)
  end

  #check how many customers are going to watch a certain film
  #e.g. "where title = 'Die Hard' ";
  def cust_film_choice_count()
    sql = "select distinct c.name, f.title, count(*) from customers c inner join
    tickets t on (c.id = t.customer_id)
    inner join films f on (t.film_id = f.id)
    where title = '$?' Group by name,title order by count(*) desc"
    values = [@id]
    data = SqlRunner.run(sql, values)
    return Customer.map_items(data)
  end

  #READ
  def self.all()
    sql = "SELECT * FROM films"
    film_data = SqlRunner.run(sql)
    return Film.map_items(film_data)
  end

  #decrease the funds of the customer by the price of ticket -necessary??
  def remaining_cust_funds()
    film_choice = self.all()
    film_price = film_choice.map{|film| film_choice.price}
    total_left = film_price.sum
    return @funds - total_left
  end

  #Delete function
  def delete()
    sql = "DELETE * FROM films where id = $1"
    values = [@id]
    SqlRunner.run(sql, values)
  end

  #Delete function
  def self.delete_all()
    sql = "DELETE FROM films"
    SqlRunner.run(sql)
  end

  def self.map_items(film_data)
    result = film_data.map { |film| Film.new( film ) }
    return result
  end

end
