require_relative("../db/sql_runner")
require_relative("customer")
require_relative("ticket")
require_relative("film")

class Screening
  attr_reader :id
  attr_accessor :showing_time
  def initialize( options )
    @id = options['id'].to_i if options['id']
    @showing_time = options['showing_time']
  end

  #CREATE
  def save()
    sql = "INSERT INTO screenings
    (  showing_time)
    VALUES
    (  $1)
    RETURNING id"
    values = [ @showing_time]
    show = SqlRunner.run( sql, values ).first
    @id = show['id'].to_i
  end

  #UPDATE function
  def update()
    sql = "UPDATE screenings SET (showing_time) = ($1 WHERE id = $2)"
    values = [@showing_time,  @id]
    SqlRunner.run(sql, values)
  end

  #READ
  def self.all()
    sql = "SELECT * FROM screenings"
    screen_data = SqlRunner.run(sql)
    return Screening.map_items(screen_data)
  end

  #finds out what is the most popular time (most tickets sold) for a given film.
  def peak_times()
    sql = "select distinct count(t.id) as tickets, f.title, MAX(s.showing_time) as peak_time from screenings s inner join tickets t ON
    (t.id = s.id) JOIN films f on (f.id = t.id) where t.id = f.film_id GROUP BY showing_time, t.id , f.title order by count(t.id), max(s.showing_time) desc"
    values = [@id]
    screen_data = SqlRunner.run(sql, values)
    return Screening.map_items(screen_data)
  end

  #READ
  def self.map_items(show_data)
    result = show_data.map { |show| Screening.new(show) }
    return result
  end


  #Delete function
  def delete()
    sql = "DELETE * FROM screenings where id = $1"
    values = [@id]
    SqlRunner.run(sql, values)
  end

  #Delete function
  def self.delete_all()
    sql = "DELETE FROM screenings"
    SqlRunner.run(sql)
  end

  #Find screen time

  # def self.find(id)
  #   sql = "SELECT * FROM screenings WHERE id = $1"
  #   SqlRunner.run(sql)
  #   values = [id]
  #   results = SqlRunner.run(sql, values)
  #   db.close()
  #   screen_hash = results.first
  #   screen = Screnning.new(screen_hash)
  #   return screen
  # end


end
