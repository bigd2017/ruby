require_relative("../db/sql_runner")
require_relative("customer")
require_relative("film")
require_relative("screening")

class Ticket

  attr_reader :id
  attr_accessor :film_id, :customer_id

  def initialize( options )
    @id = options['id'].to_i if options['id']
    @film_id = options['film_id'].to_i
    @customer_id = options['customer_id'].to_i
  end

#CREATE
  def save()
    sql = "INSERT INTO tickets (film_id, customer_id) VALUES ($1, $2) RETURNING id"
    values = [@film_id, @customer_id]
    casting = SqlRunner.run(sql, values)[0];
    @id = casting['id'].to_i
  end
#UPDATE
  def update()
    sql = "UPDATE stars SET film_id, customer_id) = ($1, $2) WHERE id = $3"
    values = [@film_id, @customer_id, @id]
    SqlRunner.run(sql, values)
  end

  def films()
    sql = "SELECT * FROM films WHERE id = $1"
    values = [@film_id]
    film = SqlRunner.run(sql, values).first
    return Film.new(film)
  end
  #READ
  def customers()
    sql = "SELECT * FROM customers WHERE id = $1"
    values = [@star_id]
    cust= SqlRunner.run(sql, values).first
    return Customer.new(cust)
  end
  #READ
  def self.all()
    sql = "SELECT * FROM tickets"
    data = SqlRunner.run(sql)
    return data.map{|ticket| Ticket.new(ticket)}
  end

  def delete()
    sql = "DELETE * FROM tickets where id = $1"
    values = [@id]
    SqlRunner.run(sql, values)
  end

  def self.delete_all()
    sql = "DELETE FROM tickets"
    SqlRunner.run(sql)
  end


end
