require_relative("../db/sql_runner")
require_relative("film")
require_relative("ticket")
require_relative("screening")

class Customer
  attr_reader :id
  attr_accessor :name, :funds, :total
  def initialize( options )
    @id = options['id'].to_i if options['id']
    @name = options['name']
    @funds = options['funds'].to_i
  end

  def save()
    sql = "INSERT INTO customers
    (  name,funds)
    VALUES
    (  $1,$2)
    RETURNING id"
    values = [@name, @funds]
    cust= SqlRunner.run( sql, values ).first
    @id = cust['id'].to_i
    hash_containing_id = SqlRunner.run(sql, values)[0]
    @id = hash_containing_id['id'].to_i
  end

  def update()
    sql = "UPDATE customers SET (name, funds, total) = ($1, $2 WHERE id = $3)"
    values = [@name, @funds,@id]
    SqlRunner.run(sql, values)
  end

  def delete()
    sql = "DELETE * FROM customers where id = $1"
    values = [@id]
    SqlRunner.run(sql, values)
  end

  #Delete
  def self.delete_all()
    sql = "DELETE FROM customers"
    SqlRunner.run(sql)
  end

  def self.all()
    sql = "SELECT * FROM customers"
    cust_data = SqlRunner.run(sql)
    return Customer.map_items(cust_data)
  end

  #Read
  def self.map_items(cust_data)
    result = cust_data.map { |customer| Customer.new( cust_data) }
    return result
  end

  #Check how many tickets were bought by a customer for all film(s)
  def cust_no_tickets_bought()
    sql = "select distinct c.id, c.name, f.title, count(*) as no_Tickets from customers c inner join
    tickets t on (c.id = t.customer_id)
    inner join films f on (t.film_id = f.id)
    group by name, c.id, title order by count(*) desc"
    data_count = SqlRunner.run(sql)
    return Customer.map_items(data_count)
  end

  #decrease the funds of the customer by the price of ticket
  def count_cust_films()
    sql = "select c.id ,c.name,f.price, MAX(c.funds - f.price) as fund_less_price from customers c
    inner join films f on (c.id = f.id) group by c.id, name, price, funds order by  price, funds"
    funds_left = SqlRunner.run(sql, values)
    return Customer.map_items(funds_left)
  end



end
