<h3>CodeClan Cinema</h3>

Create a system that handles bookings for our newly built cinema! It's enough if you can call your methods in pry.

Your application should have:

Table customers:
name,
funds

Table films:
title,
price

Table tickets:
customer_id,
film_id
screen_id

Table screenings:
id,
showing_times
-----------

The application should be able to:

1: Create customers, films and tickets.
2: CRUD actions (create, read, update, delete) customers, films and tickets.
3: Show which films a customer has booked to see, and see which customers are coming to see one film.

Basic extensions:
1: Buying tickets should decrease the funds of the customer by the price.
2: Check how many tickets were bought by a customer.
3: Check how many customers are going to watch a certain film.

Advanced extensions:

1:Create a screenings table that lets us know what time films are showing.
2:Write a method that finds out what is the most popular time (most tickets sold) for a given film
3:Limit the available tickets for screenings.

Add any other extensions you think would be great to have at a cinema!
