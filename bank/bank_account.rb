
class BankAccount


  attr_accessor :name, :balance, :type
  def initialize(name, balance, type)
    @name = name
    @type = type
    @balance = balance
  end

  def pay_in(balance)
    @balance += balance
  end

  def pay_monthly_fee
   @balance -= 10 if @type == 'personal'
   @balance -= 50 if @type == 'business'
  end

end
  # def holder_name( )
  #   return  @holder_name
  #
  # end
  # def balance( )
  #   return @balance
  # end
  #
  # def type()
  #   return @type
  # end

  # def set_holder_name(new_name)
  #   @holder_name = new_name
  # end
  #
  #
  # def set_balance(new_balance)
  #   total = 50
  #   p    @balance =  new_balance
  #   p    balance -=    total
  # end

  # def pay_monthly_fee()
  # @balance -=50
  #
#   end
#
# end
