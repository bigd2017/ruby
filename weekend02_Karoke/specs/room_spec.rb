require("minitest/autorun")
require_relative("../song")
require_relative("../guest")
require_relative("../room")
require("minitest/rg")


class RoomTest < MiniTest::Test

  def setup()
    #entry fee & name of room
    @room1 = Room.new("Blue Jazz Premium Deluxe",75.0, 10)
    @room2 = Room.new("Fun Times",40.0, 5)
    @room3 = Room.new("Rock Star room",35.0, 3)
    #guest name & money (wallet)
    @guest1 = Guest.new("David", 100.0)
    @guest2 = Guest.new("James Doe",30.0)
    @guest3 = Guest.new("Bruce Smith", 40.0)
    @guest4 = Guest.new("Jeremy Korbin", 20.0)
    #song name
    @song1 = Song.new("Solo: Clean Bandit")
    @song2 = Song.new("Pump Up the Jam: Technotronic")
    @song3 = Song.new("One More Time")
    @song4 = Song.new("Gorillaz: Feel Good")
    @song5 = Song.new("Blur: Parklife")
    @song6 = Song.new("One Day Like This")
    @fav_playlist = Room.new("One Day Like This", "One More Time","Solo: Clean Bandit")

  end

  #check room has a name associated
  def test_room_has_a_name
    assert_equal("Fun Times", @room2.room_name())
  end

  #test that room has a fee/price
  def test_room_has_a_fee
    assert_equal(40.0, @room2.room_cost())
  end

  #assert guests can be checked in
  def test_can_check_in_guest
    fill = [@guest1, @guest2]
    assert_equal(2, @room1.check_in(fill.length()))

  end

  #test that a guest can be checked out of a room
  def test_guest_can_check_out
    @room3.check_in(@guest1)
    @room3.check_in(@guest2)
    @room3.check_in(@guest3)
    @room3.check_out_guest(@guest1)
    assert_equal(2, @room3.guest_count)
  end

  #test a guest can be checked-in to room
  def test_room_has_guest_inside
    @room2.check_in(@guest2)
    assert_equal(1, @room2.guest_count)
  end

  #test songs count
  def test_room_contains_song_list
    @room2.add_songs(@song3)
    assert_equal(1, @room2.song_count)
  end

  #test room can add to songs
  def test_song_can_be_added_to_list
    song_list = [@song1, @song1]
    assert_equal(2, @room1.add_songs(song_list.length()))
  end

  #test room can remove a song from list
  def test_song_can_be_removed
    @room1.add_songs(@song1)
    @room1.add_songs(@song2)
    @room1.add_songs(@song3)
    @room1.remove_song(@song1)
    assert_equal(2, @room1.song_count)
  end

  #test if room has reached full capacity
  def test_room_capacity_is_full
    @room3.check_in(@guest1)
    @room3.check_in(@guest1)
    @room3.check_in(@guest1)
    @room3.check_in(@guest1)
    assert_equal("Sorry, Room Full", @room3.full_capacity(@guest1))
  end

  # #validate room is not full- spaces left
  def test_room_capacity_is_Not_full
    @room1.check_in(@guest1)
    @room1.check_in(@guest1)
    assert_equal("Room has space", @room1.full_capacity(@guest1))
  end

  #validate that a guest has sufficient money for venue entry charge
  def test_guest_can_pay_room_fee
    guest= @guest_1
    @guest1.pay_charge(@guest1, @room1.room_cost)
    assert_equal(25, @guest1.money)
  end

  #validate that a guest has insufficient money for venue entry
  def test_guest_can_pay_room_fee
    guest= @guest_2
    @guest2.pay_charge(@guest2, @room1.room_cost)
    assert_equal(-45,@guest2.money)
  end

  #validate guest can choose random song from playlist
  def test_guest_can_choose_random_song
    @room2.add_songs(@song1)
    @room2.add_songs(@song2)
    @room2.add_songs(@song3)
    @room2.add_songs(@song4)
    @room2.add_songs(@song5)
    songs = [@song1, @song2, @song3, @song4,@song5]
    change = @room2.surprise_song(songs)
    assert_equal(true, change)
  end

  #test guest's fav song on playlist -return if true
  def test_guest_fav_song_on_playlist
    @room2.add_songs(@song1)
    @room2.add_songs(@song2)
    @room2.add_songs(@song3)
    @room2.add_songs(@song4)
    @room2.add_songs(@song5)
    @room2.add_songs(@song6)
    songs = [@song1, @song2, @song3, @song4,@song5, @song6]
    playlist = @room2.favourite_song(@song6)
    assert_equal("whoo!", playlist)
  end

  def test_guest_fav_song_not_on_playlist
    @room2.add_songs(@song1)
    @room2.add_songs(@song2)
    @room2.add_songs(@song4)
    @room2.add_songs(@song5)
    @room2.add_songs(@song6)
    songs = [@song1, @song2, @song4,@song5, @song6]
    playlist = @room2.favourite_song(@song3)
    assert_equal(false, playlist)
  end


end
