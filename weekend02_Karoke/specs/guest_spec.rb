require("minitest/autorun")
require_relative("../song")
require_relative("../guest")
require_relative("../room")
require("minitest/rg")


class GuestTest < MiniTest::Test


  def setup()
    @guest1 = Guest.new("David", 100.0)
    @guest2 = Guest.new("James Doe",30.0)

  end

  #check guest has a name
  def test_guest_has_name
    assert_equal("David", @guest1.name())
  end

  #test that guest has money to pay entry fee
  def test_guest_has_money
    assert_equal(30.0, @guest2.money())
  end

  

end
