require("minitest/autorun")
require_relative("../guest")
require_relative("../room")
require_relative("../pub_office")
require("minitest/rg")

class TestOffice < MiniTest::Test


  def setup()
    @office = Office.new(150.0, "Karoke KodeKlan")

  end
  #

  def test_pub_office_has_bank_money
    assert_equal(150.0, @office.bank())
  end


  def test_pub_office_has_name
    assert_equal("Karoke KodeKlan", @office.name())
  end



end
