require("minitest/autorun")
require_relative("../song")
require_relative("../guest")
require_relative("../room")
require("minitest/rg")


class SongTest < MiniTest::Test


  def setup()
    @song1 = Song.new("Solo: Clean Bandit")
    @song2 = Song.new("Pump Up the Jam: Technotronic")
    @song3 = Song.new("One More Time: Daft Punk")
    @jukebox = [@song1, @song2, @song3, @song4, @song5]
  end

  def test_song_has_name
    assert_equal("Solo: Clean Bandit", @song1.name())
  end


end
