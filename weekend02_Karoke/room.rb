
class Room


  attr_reader :room_name, :room_cost, :max_capacity

  #initalization of instance variables
  def initialize(room_name, room_cost, max_capacity)
    @room_name = room_name
    @room_cost = room_cost
    @song_list = []
    @guest_in_room = []
    @max_capacity = max_capacity
  end

  #check the room count (a guest has been added)
  def guest_count()
    return @guest_in_room.length()
  end

  #check-in guest to room
  def check_in(guest)
    @guest_in_room << guest
    return guest
  end

  #check-out guest from room
  def check_out_guest(name_guest)
    @guest_in_room.delete(name_guest)
  end

  #check room has songs
  def song_count()
    return @song_list.length()
  end

  #add a song to song_list in room
  def add_songs(song)
    @song_list << song
    return song
  end

  #remove song from room
  def remove_song(song)
    @song_list.delete(song)
  end

  # check if the room capacity is full
  def full_capacity(guests)
    # for max in room
    if @guest_in_room.length()> @max_capacity
      return "Sorry, Room Full"
    else
      return "Room has space"
    end
  end

  #return favourite song on playlist, false otherwise
  def favourite_song(song)
    if @song_list.include?(song)
      return p "whoo!"
    else p false
    end
  end

  #return a 'random' song from guest's playlist
  def surprise_song(song)
    song.collect { |x| x.to_s }
    temp = song.shuffle #store shuffled song in variable
    return true if song != temp #compare current with previous song in list
  else return false
  end



end
