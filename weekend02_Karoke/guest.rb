
class Guest

  attr_reader  :name, :money
  #initalization of instance variables
  def initialize(name, money)
    @name = name
    @money = money
  end

  # guest's money decreased
  def pay_charge(guest, fee)
    @money -= fee
  end


end
