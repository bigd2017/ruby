


class Instrument



  attr_reader :name, :type
  def initialize(name, type)
    @name = name
    @type  = type

  end

  # def has_name(name)
  #   p "name is : #{name}"
  # end
  # def has_type(type)
  #   return  "type is : #{type}"
  # end


  def make_sound(song)
    if @type == "piano"
      return "plink plonk... i'm playing #{song}"
    else
      return "i'm playing #{song}!"
    end
  end

end
