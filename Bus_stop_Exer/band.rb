class Band

  attr_reader :name, :musicians

  def initialize(name, musicians)
    @musicians = musicians
    @name = name
  end


  def perform(song_title)
    songs  = []
    for music in @musicians
      songs << music.play_song(song_title)
    end
    return songs
  end
  
end
