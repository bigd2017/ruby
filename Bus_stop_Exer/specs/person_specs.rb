require_relative("../person")
require("minitest/rg")
require("minitest/autorun")

class TestPerson < MiniTest::Test

  def setup
    @persons = Person.new("david", 19)
  end

  def test_person_name_age
    assert_equal("david", @persons.name)
    assert_equal(19, @persons.age)
  end



end
