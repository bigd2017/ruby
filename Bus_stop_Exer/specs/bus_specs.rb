require_relative("../bus")
require_relative("../person")
require("minitest/rg")
require("minitest/autorun")

class TestBus < MiniTest::Test

  def setup
    @bus = Bus.new(23, "Morningside")
    @person = Person.new("Mark", 30)
    @person2 = Person.new( "James", 10)

  end


  def test_has_name
    assert_equal(23, @bus.number)
  end

  def test_bus_count
    assert_equal(0, @bus.passengers.count)
  end


  def test_add_passengers
    @bus.add_passenger(@person)
    assert_equal(1, @bus.passengers.count)
  end


  def test_bus_count_has_two_persons
    @bus.add_passenger(@person2)
    assert_equal(1, @bus.passengers.count)
    @bus.remove_passenger(@person2)
    assert_equal(0, @bus.passengers.count)
  end

  def test_remove_all_passengers
    @bus.add_passenger(@person)
    @bus.add_passenger(@person2)
    # assert_equal(2, @bus.passengers.count)
    @bus.remove_all_passengers(@person)
    assert_equal(0, @bus.passengers.count)
  end

end
