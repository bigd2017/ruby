require_relative("../instrument")
require("minitest/rg")
require("minitest/autorun")

class TestInstrument < MiniTest::Test
  def setup
    @instrument = Instrument.new("Les Paul", "guitar") #name, type
  end


  def test_has_name
    assert_equal("Les Paul", @instrument.name)
  end


  def test_has_type
    assert_equal("guitar", @instrument.type)
  end

  def test_can_make_song
    assert_equal("i'm playing stairway to heaven!", @instrument.make_sound("stairway to heaven"))
  end

  def test_can_make_sound_piano
    piano = Instrument.new("Yamaha", "piano")
    assert_equal("plink plonk... I'm playing Ordinary people", piano.make_sound("Ordinary people"))
  end

end
