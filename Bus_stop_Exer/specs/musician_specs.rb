require_relative("../musician")
require_relative("../instrument")
require("minitest/rg")
require("minitest/autorun")

class MusicianTest < MiniTest::Test

  def setup
    strat = Instrument.new("Fender strat", "guitar")
    grand = Instrument.new("bechstein", "piano")
    @musician1 = Musician.new("Jimi Hendrix",strat)
    @musician2 = Musician.new("Freddie Mercury", grand)
  end


  def test_musician_has_name
    assert_equal("Jimi Hendrix", @musician1.name)
  end

  def test_can_play_song_piano
    assert_equal("I'm playing Hey Joe!", @musician1.play_song("Hey Joe"))
  end

  def test_musician_can_play_song
    assert_equal("plink... plonk singing Bohemian Rhapsody!", @musician2.play_song("Bohemian Rhapsody"))
  end



end
