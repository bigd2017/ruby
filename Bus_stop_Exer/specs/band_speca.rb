require_relative("../band")
require("minitest/rg")
require("minitest/autorun")
require_relative("../musician")
require_relative("../instrument")


class TestBand < MiniTest::Test

  def setup
    piano = Instrument.new("Jimi Hendrix","piano")
    drums = Instrument.new("Freddie Mercury", "drums")

    jack = Musician.new("Jimi Hendrix",piano)
    jill = Musician.new("Freddie Mercury", drums)

    musicians = [jack, jill]
    @whitestripes = Band.new("gorillaz", musicians)

  end

  def test_band_has_name
    assert_equal("gorillaz", @whitestripes.name)
  end

  def test_can_play_song
    expected  = ["plink plonk i'm playing My Doorbell", "i'm playing My Doorbell!"]
    assert_equal(expected, @whitestripes.perform("My Doorbell"))
  end

end
