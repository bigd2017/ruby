class Person
  attr_reader :name, :age

  def initialize(name, age)
    @name = name
    @age = age
  end


  def person_names(name)
    return  "david, #{name} "
  end

  def person_age(age)
    return  "19, #{age}"
  end

end
