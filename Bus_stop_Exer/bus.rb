



class Bus
  attr_reader :number, :route, :passengers

  def initialize( number, route)

    @number = number
    @route = route
    @passengers = []
  end
  # The Bus should have a drive method that returns a string (e.g. "Brum brum").

  def bus_drives()
    return  "Brum brum"
  end

  def add_passenger(person)
    @passengers.push(person)

  end

  def remove_passenger(person)
    @passengers.delete(person)

  end

  #
  def remove_all_passengers(person)
    for passenger in @passengers
      if passenger == person
        @passengers.delete(passenger)
      end
    end
  end

end
