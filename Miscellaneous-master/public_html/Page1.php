<!DOCTYPE html>
<html lang="en">
<head>
 
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      width: 60%;
      margin: auto;
  }
  </style>
        <div class="w3-container w3-light-brown">
      
 <center><h1 style="color:brown;">Cramond Island Handicap, 8 June 2017</h1></center>
</head>
<body>

<div class="container">
  <br>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
     
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">

      <div class="item active">
        <img src="Capture.PNG" alt="Cramond" width="460" height="345">
        <div class="carousel-caption">
          <h2><font color="brown">HBT Midsummer Cramond Island Handicap</font></h2>
    
       
        </div>
      </div>

      <div class="item">
        <img src="Cramond Island.jpg" alt="cramondisl" width="460" height="345">
        <div class="carousel-caption">
        
         
        </div>
      </div>
    
      <div class="item">
        <img src="Cramond_WW2.jpg" alt="ww2"  width="460" height="345">
        <div class="carousel-caption">
      
        </div>
      </div>

       <div class="item">
        <img src="DSC.jpg" width="460" height="345">
        <div class="carousel-caption">
        
        </div>
      </div>
      
      <div class="item">
        <img src="sunset.jpg" alt="sunset" width="460" height="345">
        <div class="carousel-caption">
        
        </div>
      </div>
         
   
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>

</body>

<html>
     

 
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>
<body style="background-color:#93C681;">
  <div class="w3-container w3-light-brown">
   
<html>
    <div class="container-fluid">
  <h3> <b>Start: Cramond promenade (start of Edin. Parkrun/kraprun)</b></h3>
<body>
  <h3> <b>Instructions:</b></h3>
  <b>Route Detail:</b> Head west along promenade onto beach in direction of causeway. Run towards the island and <b>TURN LEFT</b>  in clockwise direction around island once reaching island. <b>STAY </b> on beach and <b>TURN RIGHT</b>  at marker <b>JUST BEFORE</b>  the abandoned/ruined croft.  Follow the trail and turn Right as indicated by flour markers. Continue along trod until reaching Trig point at island summit.  Head straight back down along the causeway in same direction you came. Finish on beach area.
 <b><i>Footwear: Studs/trainers</i></b>
  </body>
      <br></br>
  </html>
      <div class="container-fluid">
<center><iframe src="https://docs.google.com/forms/d/e/1FAIpQLSeOxutMpNOFmFlHn7w7yGkKPyHT-5b08_CNNAEASYCq7zV0Bw/viewform?embedded=true" class="container" scrolling="yes" width="800" height="600" frameborder="1" marginheight="0" marginwidth="0">Loading...</iframe></center>
<hr>
  </div>
   
        <center> <h2 style="color:brown;"><b>Course Route</b></h2> </center>
  
        <center>  <iframe src="//www.mapometer.com/embed/route/4551432?view=opensm&units=miles&notes&graph"   class="container" scrolling="no" sandbox="allow-same-origin allow-scripts allow-top-navigation" width="800" height="600" style="border:1px solid grey;"></iframe></center>
              </div>
              

              </body>
</html>