require_relative('../models/artist')
require_relative('../models/exhibition')

artist1 = Artist.new({'first_name' => 'David', 'second_name' => 'Hume'})
artist2 = Artist.new({'first_name' => 'Charles', 'second_name' => 'Beard'})
artist3 = Artist.new({'first_name' => 'Perfidous', 'second_name' => 'Albion'})
artist4 = Artist.new({'first_name' => 'Eric', 'second_name' => 'Hobsbawm'})

artist1.save
artist2.save
artist3.save
artist4.save

exhibit1 = Exhibition.new({
  'artists_lname' => 'hermione',
  'title' => 'early history',
  'artist_id' => artist1.id
     })
exhibit2 = Exhibition.new({
    'artists_lname' => 'owen',
    'title' => 'early twentieth century: age of extremes ',
    'artist_id' => artist2.id
     })
exhibit3 = Exhibition.new({
      'artists_lname' => 'roger',
      'title' => 'industrial britain',
      'artist_id' => artist3.id
      })
exhibit4 = Exhibition.new({
        'artists_lname' => 'ealy modern',
        'title' => 'scientific revolution',
        'artist_id' => artist4.id
      })

        exhibit1.save
        exhibit2.save
        exhibit3.save
        exhibit4.save
