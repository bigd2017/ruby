DROP TABLE exhibitions;
DROP TABLE artists;

CREATE TABLE artists (
  id SERIAL8 primary key,
  first_name VARCHAR(255),
  second_name VARCHAR(255)
);

CREATE TABLE exhibitions (
  id SERIAL8 primary key,
  artists_lname VARCHAR(255),
  title varchar(255),
  artist_id INT8 REFERENCES artists(id)
);
