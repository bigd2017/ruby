require_relative('../db/sql_runner')

class Exhibition

  attr_accessor :artists_lname, :title, :artist_id
  attr_reader :id

  def initialize(options)
    @id = options['id'].to_i if options['id']
    @title = options['title']
    @artists_lname = options['artists_lname']
    @artist_id = options['artist_id'].to_i
  end

  def save()
    sql = "INSERT INTO exhibitions
    (
      artists_lname,
      title,
      artist_id
    )
    VALUES
    (
      $1, $2, $3
    )
    RETURNING id"
    values = [ @artists_lname, @title, @artist_id]
    result = SqlRunner.run(sql, values)
    id = result.first['id']
    @id = id
  end

  def exhibition()
    exhibit = Exhibition.find(@artist_id)
    return exhibit
  end

  def update()
    sql = "UPDATE exhibitions
    SET
    (
      artists_lname,
      title,
      artist_id

      ) =
      (
        $1, $2, $3
      )
      WHERE id = $4"
      values = [ @artists_lname, @title, @artist_id, @id]
      SqlRunner.run(sql, values)
    end

    def delete()
      sql = "DELETE FROM exhibitions
      WHERE id = $1"
      values = [@id]
      SqlRunner.run(sql, values)
    end

    def self.all()
      sql = "SELECT * FROM exhibitions"
      exhibition_data = SqlRunner.run(sql)
      exhibitions = map_items(exhibition_data)
      return exhibitions
    end

    def self.map_items(exhibition_data)
      return exhibition_data.map { |exhibition| Exhibition.new(exhibition) }
    end

    def self.find(id)
      sql = "SELECT * FROM exhibitions
      WHERE id = $1"
      values = [id]
      result = SqlRunner.run(sql, values).first
      exhibition = Exhibition.new(result)
      return exhibition
    end

    def format_name
      return "#{@artists_lname.capitalize} #{@title.capitalize}"
    end


  end
