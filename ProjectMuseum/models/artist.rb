require_relative('../db/sql_runner')

class Artist

  attr_reader :id, :first_name, :second_name

  def initialize(options)
    @id = options['id'].to_i if options['id']
    @first_name = options['first_name']
    @second_name = options['second_name']
  end

  def save()
    sql = "INSERT INTO artists
    (
      first_name,
      second_name
    )
    VALUES
    (
      $1, $2
    )
    RETURNING id"
    values = [@first_name, @second_name]
    result = SqlRunner.run(sql, values)
    id = result.first["id"]
    @id = id.to_i
  end

  def self.find(id)
    sql = "SELECT * FROM artists
    WHERE id = $1"
    values = [id]
    result = SqlRunner.run(sql ,values).first
    artist = Artist.new(result)
    return artist
  end

  def self.all()
    sql = "SELECT * FROM artists"
    artist_data = SqlRunner.run(sql)
    artists = map_items(artist_data)
    return artists
  end

  def self.map_items(artist_data)
    return artist_data.map { |artist| Artist.new(artist) }
  end

end
