require('sinatra')
require('sinatra/contrib/all')
require_relative('models/exhibition')
require_relative('models/artist')
also_reload('./models/*')

get '/exhibition' do
  @exhibitions = Exhibition.all
  erb(:index)
end



get '/exhibition/new' do
  @artists = Artist.all
  erb(:new)
end

get '/exhibition/:id' do
  @exhibitions = Exhibition.find(params['id'])
  erb(:show)
end

post '/exhibition' do
  Exhibition.new(params).save
  redirect to '/exhibition'
end



get '/exhibition/:id/edit' do
  @artists = Artist.all
  @exhibition = Exhibition.find(params['id'])
  erb(:edit)
end

post '/exhibition/:id' do
  exhibit = Exhibition.new(params)
  exhibit.update
  redirect to "/exhibition/#{params['id']}"
end

post '/exhibition/:id/delete' do
  exhibit = Exhibition.find(params['id'])
  exhibit.delete
  redirect to '/exhibition'
end
