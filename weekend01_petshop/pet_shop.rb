# 1. For a given person, return their name
def pet_shop_name(shopName)
  shopName[:name]
end

#2 return the total in petshop
def total_cash(shopName)
  shopName[:admin][:total_cash]
end

#3: add/remove 10 GDP to totalcash in shop [2 questions]

def add_or_remove_cash(shop, amnt)
  shop[:admin][:total_cash] += amnt
end

#5: return no. of pets pets_sold
def pets_sold(sold)
  sold[:admin][:pets_sold]
end

#6: add two pets to no of pets sold
def increase_pets_sold(shop,pet)
  shop[:admin][:pets_sold] += pet
end

#7: return no in stock_count
def stock_count(petCount)
  petCount[:pets].count
end

#8/9: find british shorthairs
def pets_by_breed(shop, breed)
  pet_breed = []
  for pet in shop[:pets]
    if pet[:breed] == breed
      pet_breed.push(pet)
    end
  end
  return pet_breed
end

#10/11: find 'Fred's'
def find_pet_by_name(pshop, name)
  found = nil
  for pet in pshop[:pets]
    if pet[:name] == name
      found = pet
    end
  end
  return found
end
#12: remove arthurs
def remove_pet_by_name(shop, name)
  for pet in shop[:pets]
    if pet[:name] == name
      shop[:pets].delete(pet)
    end
  end
end

#13: add pet to pet stock
def add_pet_to_stock(shop, addPet)
  pet_breed = []
  shop[:pets].push(addPet)
end

#14 return customer cash
def customer_cash(cust_cash)
  cust_cash[:cash]
end

#15:remove £100 from 1st customer's cash
def remove_customer_cash(customer, cash)
  customer[:cash] -= cash
end

#16: test no of customers' pets
def customer_pet_count(cust_pet)
  cust_pet[:pets].count
end

#17: add 1 pet to first customer
def add_pet_to_customer(customer, addPet)
  customer[:pets].push(addPet)
end

def decrease_pets_sold(shop,sold)
  shop[:pets].delete(pet)
end


#--------------------------
#OPTIONAL STUFF...
#--------------------------
# 18/19:test whether sufficient/insufficient funds
#
def customer_can_afford_pet(custCash, pet)
  return custCash[:cash] > pet[:price]

end

def sell_pet_to_customer(shop, pet, customer)
  return if (pet == nil)
  if customer_can_afford_pet(customer, pet)
    add_pet_to_customer(customer, pet)
    increase_pets_sold(shop,1)
    remove_customer_cash(customer, pet[:price])
    add_or_remove_cash(shop, pet[:price])
  end
end
