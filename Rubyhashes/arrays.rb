#arrays

fruits = ['apple','banana', 'orange', 'raspberry']
veggies = ['potato', 'cabbage', 'brocolli', 'tomatoe']
#p fruits
#p fruits[-2]
# p fruits.last

my_arr = Array.new()
#same as
p my_arr = [[1,2,3,4,5],[1,2,3],[1,2,3,4,5,6,7,8,9]]
p my_arr.flatten.sort()

fruits.push('pear')
#p fruits
#remove element
fruits.pop(2)
#p fruits

fruits << 'lemon'
#p fruits

#add new element
fruits.unshift('apple')
#p fruits


fruits_numbs_array = ['apple',1, 'orange', 5]
#p fruits_numbs_array

#veggies.each_index {|i| puts " #{i}"}

p fruits.sort!()
p fruits.shuffle!()
