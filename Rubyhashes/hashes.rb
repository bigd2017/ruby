my_hash = Hash.new()
my_sec_hash = {}

meals = {
  "breakfast" => "yoghurt",
  "lunch" => "steak",
  "dinner"=> "chicken"
}
# p meals
# p meals['dinner']



meals = Hash.new("nowt")
# p meals ["supper"]

meals["supper"] = "pancakes"
meals["lunch"] = "chargrille chicken"
#p meals


meals = {
  :breakfast => "yoghurt",
  :lunch => "Greggs"
}
# p meals[:breakfast]
#meals.delete("breakfast")


#[TASK:] Make a hash with a key of a persons name and the
#value as their pocket money.
#Try updating and deleting items from it.

names = Hash.new()

names = {
  "David" => "£10",
  "James" => "£5",
  "Chen" => "£50.0",
  "Mark" => "£10.0"
}



 #names["Henry"] = "£1000.0"
 #p names

 #names.delete("James")
 #p names

# p names.keys
# p names.values


countries = {
  UK: {
    capital: "London",
    population: "800,000,000"
  },

  France:{
    capital:"Paris",
    population: "800, 000, 000"
    } ,
  Germany: {
    capital:"Berlin",
    population: 400000000
    }
}
# p countries[:UK][:capital]
# p countries[:France][:population]
# p countries[:Germany][:population]

#
# Make a hash containing the avengers Iron Man and Hulk. Each avenger has a name
# (Tony Stark and Bruce Banner) and a set of moves with an attack power.
# Iron man can punch (10) and kick (100) and Hulk can smash (1000) and roll (500).

avengers = Hash.new()
avengers = {
  IronMan: {
    name: "Tony Stark",
    kick: 100,
    punch: 10
  },

  Hulk: {
    name: "Bruce Banner",
    smash: 1000,
    roll: 500
  }
}
 p avengers[:IronMan][:name]
 p avengers[:IronMan][:kick]
p avengers[:IronMan][:punch]
 p avengers[:IronMan]


p avengers[:Hulk][:name]
p avengers[:Hulk][:smash]
p avengers[:Hulk][:roll]
p avengers[:Hulk]
