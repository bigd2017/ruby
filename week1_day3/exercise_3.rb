united_kingdom = [
  {
    name: "Scotland",
    population: 5295000,
    capital: "Edinburgh"
  },
  {
    name: "Wales",
    population: 3063000,
    capital: "Swansea"
  },
  {
    name: "England",
    population: 53010000,
    capital: "London"
  }
]
# Change the capital of Wales from "Swansea" to "Cardiff"
united_kingdom[1][:capital] = "cardiff"
# # Create a Hash for Northern Ireland
# # and add it to the united_kingdom array
united_kingdom.push({

    name: "NI",
    population: 1811000,
    capital: "Belfast"
})
p united_kingdom.flatten

# Use a loop to print the names of all the countries in the UK
p united_kingdom.each{ |x, y| puts "capital of #{x}  #{y}" }
for country in united_kingdom
  p " country: #{country[:name] }"
end
# Use a loop to find the total population of the UK.
def total_pop(array)
  total = 0
  for people in array
    total += people[:population]
  end
  return   total.to_s() + " total in uk"
end
p  total_pop(united_kingdom)
