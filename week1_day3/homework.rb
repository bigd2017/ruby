
stops = [ "Croy", "Cumbernauld", "Falkirk High", "Linlithgow", "Livingston", "Haymarket" ]

#Add "Edinburgh Waverley" to the end of the array
stops.push('Edinburgh Waverley')
p stops

# Add "Polmont" at the appropriate point (between "Falkirk High" and "Linlithgow")
p stops[2]<< "Polmont"


# Work out the index position of "Linlithgow"
p stops.index("Linlithgow")

# Remove "Livingston" from the array using its name
stops.delete('Livingston')
p stops

# Add "Glasgow Queen St" to thfore start of the array
stops.unshift('Glasgow Queen st')
p stops

# Delete "Cumbernauld" from the array by index
stops.delete('Cumbernauld')
p stops
# or
stops.delete_at(2)
# How many stops there are in the array?
p stops.length
stops.count
stops.size
# How many ways can we return "Falkirk High" from the array?
p stops[2]
p stops[-4]
p my_stops = stops.slice!(2)
# Reverse the positions of the stops in the array
p stops.reverse

# Print out all the stops using a for loop
for stop in stops
  p stop
end
