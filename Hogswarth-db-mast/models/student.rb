require_relative('../db/sql_runner')
require('pry-byebug')

class Student

  attr_reader :id #
  attr_accessor :first_name, :second_name, :house, :age

  def initialize( options )
    @id = options['id'].to_i if options['id']
    @first_name = options['first_name']
    @second_name = options['second_name']
    @house = options['house']
    @age = options['age'].to_i
  end

  def save()
    sql = "INSERT INTO students
    (
      first_name,
      second_name,
      house,
      age
    )
    VALUES
    (
      $1, $2, $3, $4
    )
    RETURNING *"
    values = [@first_name, @second_name, @house, @age]
    stu_data = SqlRunner.run(sql, values)
    @id = stu_data.first()['id'].to_i
  end

  def update()
    sql = "UPDATE students
    SET
    (
      first_name,
      second_name,
      house,
      age
      ) =
      (
        $1, $2, $3, $4
      )
      WHERE id = $5"
      values = [@first_name, @second_name, @house, @age, @id]
      SqlRunner.run( sql, values )
    end

    def pretty_name()
      return "#{@first_name} #{@second_name}"
    end

    #find A student
    def self.find( id )
      sql = "SELECT * FROM students WHERE id = $1"
      student_h = SqlRunner.run( sql, [id] ).first #first ele in array
      result = Student.new( student_h)
      return result
    end

    #find ALL students
    def self.find_all()
      sql = "SELECT * FROM students" # no need where id = $1
      # as no value given to $1 so  won't return anything.
      student_query = SqlRunner.run( sql )
      stu_array = student_query.map { |all| Student.new(all)}#return hash array
      return stu_array
    end


    def self.delete_all()
      sql = "DELETE FROM students;"
      SqlRunner.run(sql)
    end

  end
