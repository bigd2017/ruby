-- students have to be dropped first, because they rely on houses
DROP TABLE IF EXISTS students;
DROP TABLE IF EXISTS houses;

CREATE TABLE houses (
  id serial2 PRIMARY KEY,
  name varchar(255)
);

CREATE TABLE students (
  id serial2 PRIMARY KEY,
  first_name varchar(255) NOT NULL,
  second_name varchar(255) NOT NULL,
  house varchar(255),
  age int2 NOT NULL
);
