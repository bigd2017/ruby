require( 'sinatra' )
require( 'sinatra/contrib/all' )
require_relative('models/student')
require( 'pry-byebug' )
also_reload('./models/*')


get '/student' do
  # @students better to be plural because it refers to all of them
  # it help you differentiate
  @students = Student.find_all()
  erb(:show_all)
end

get 'student/:id' do
  # and here you only work with ONE student, so keep it singular
  @student = Student.find(params[:id])
  erb(:show_all)
end
#
#
# get '/student' do
#   erb(:show_all)
# end
