require('pg')
class Bounty

  attr_accessor :favourite_weapon, :species, :homeworld, :bounty_value
  attr_reader :id
  def initialize(options)
    @id = options['id'].to_i if options['id']
    @favourite_weapon= options['favourite_weapon']
    @species = options['species']
    @homeworld = options['homeworld']
    @bounty_value =options['bounty_value'].to_i
  end

  #retrieve/extract data from db table
  def Bounty.all()
    db = PG.connect({
      dbname: 'bounty',
      host: 'localhost'
      })
      sql = "select * from bounty"
      db.prepare("all",sql)
      hunt = db.exec_prepared("all")
      db.close()
      return hunt.map {|hunt| Bounty.new(hunt)}
    end

    def Bounty.delete_all()
      db = PG.connect({
        dbname: 'bounty',
        host: 'localhost'
        })
        sql = "delete from bounty"
        db.prepare("delete_all", sql)
        db.exec_prepared("delete_all")
        db.close()
      end
      #connect db and save data cols/values in db table.
      def save()
        db = PG.connect({
          dbname: 'bounty',
          host: 'localhost'
          })
          sql = "INSERT INTO bounty (favourite_weapon,species,homeworld,bounty_value)
          VALUES ($1, $2, $3, $4) returning *"#return all
          values = [ @favourite_weapon,@species, @homeworld,@bounty_value]
          db.prepare("save", sql)
          @id = db.exec_prepared("save", values)[0]["id"].to_i #return the id fro 1st element in array [id] &
          #and store in @id variable
          db.close()
        end

        def Bounty.find_by_species(species)
          db = PG.connect({
            dbname: 'bounty',
            host: 'localhost'
            })
            sql = "select * from bounty where species = $1 "
            values = [species]
            db.prepare("find_by_species", sql)
            found = db.exec_prepared("find_by_species", values)
            db.close
            found_array = found.map{|species| Bounty.new(species)}
            return found_array
            return found
          end

          def delete ()
            db = PG.connect({
              dbname: 'bounty',
              host: 'localhost'
              })
              sql = "delete from bounty where id = $1"
              values = [@id]
              db.prepare("delete", sql)
              db.exec_prepared("delete",values)
              db.close()
            end

            def update ()
              db=PG.connect({
                dbname: 'bounty',
                host: 'localhost'
                })
                sql = "update bounty SET (favourite_weapon, species, homeworld, bounty_value)
                = ($1,$2,$3,$4) where id = $5"
                values = [@favourite_weapon,@species, @homeworld,@bounty_value, @id]
                db.prepare("update",sql)
                db.exec_prepared("update",values)
                db.close()
              end
            end
