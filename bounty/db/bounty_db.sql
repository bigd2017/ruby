DROP TABLE IF EXISTS bounty;

create table bounty(
  id SERIAL4 primary key,
  favourite_weapon varchar(255),
  species  varchar(255),
  homeworld  varchar(255),
  bounty_value INT2
);
