<h3>Ecosystem Mulitple OOP classes</h3>
<h4>Homework: week 2/day 2</h4>

<b>Bears, River, Fish</b>
We are going to build a small ecosystem! We want to create an ecosystem made up of 3 parts: bears, river and fish. You will need a class for each of these.

From this you can make instances and build your ecosystem.

How Our Ecosystem Works
A river should have a name e.g. "Amazon"
A river should hold many fish-'instances'

A fish should have a name (type)

A bear should have a name e.g. "Yogi" and a type e.g. "Grizzly"
A bear should have an empty stomach ( maybe an array ) i.e empty array
A bear should be able to take a fish from the river #bear should take fish from fish in river array

A river should lose a fish when a bear takes a fish #remove fish from array when 'eaten' by grizzly

Extensions
A bear could have a roar method
A bear could have a food_count method
A river could have a fish_count method
