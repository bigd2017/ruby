require ('sinatra')
require ('sinatra/contrib/all')
require ('pry-byebug')
require_relative ('./models/game.rb')
also_reload('models/*')

get "/" do
  erb( :home)
end

get '/rock' do
  erb( :rock)
end

get '/paper' do
  erb( :paper )
end

get '/scissors' do
  erb( :scissors )
end

get '/:player1/:player2' do
  game = Game.new(params[:player1], params[:player2])
  @result = game.play_game(params[:player1], params[:player2])
  # binding.pry/
  erb(:result)
end
