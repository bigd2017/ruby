require 'minitest/autorun'
require_relative '../models/game'
require ('minitest/rg')

class TestGame < MiniTest::Test

    def setup
      @result1 = Game.new('player1', 'player2')
    end

    def test_check_win
    assert_equal('rock wins!', @result1.play_game('rock', 'scissor'))
    end

    def test_check_win_scissor
    assert_equal('scissor wins!', @result1.play_game('scissor', 'paper'))
    end

    def test_check_win_paper
    assert_equal('paper wins!', @result1.play_game('paper', 'rock'))
    end

    def test_check_draw
      assert_equal('Draw!', @result1.play_game('paper', 'paper'))
    end



  end
