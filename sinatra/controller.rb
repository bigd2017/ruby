require 'sinatra'
require 'sinatra/reloader'
require 'sinatra/contrib/all'
require 'pry-byebug'
# also_reload('lib/**/*.rb')
require('./lib/rectangle')

get('/rectangle/new') do
  @description = "This application will determine...."
  erb(:new)
end

# As you build bigger Sinatra applications,
# try to keep the code you put in app.rb to a minimum by keeping all business logic separate!
get('/') do
  erb(:input)
end

post('/output') do
  @length = params.fetch('length')
  @width = params.fetch('width')

  rectangle = Rectangle.new(@length,@width) #instance vars.
  if rectangle.square?
    @rstring_to_display = "This is a square."
  else
    @rstring_to_display = "This is not a square."
  end
  erb(:output)
end
# Our Sinatra application can now handle two routes: / and /output.
# When a user navigates to the / route, Sinatra will respond by finding the get('/')
# block and returning input.erb.

# # Then, when a user submits a form, Sinatra responds with our new/output` route.
# We can also access them using the Hash#fetch() method like this:
# params.fetch("length" and params.fetch("width")`. -->
