class Rectangle
  def initialize(length,width)
    @length = length
    @width = width
  end
# @length and @width instance variables here will be instantiated
# and stored in each instance of a Rectangle object.
  def square?
    @length.eql?(@width)
  # if rectangle.square? #if rectanglein controller logic equals the width in square? function
  #then a square
  end
end
