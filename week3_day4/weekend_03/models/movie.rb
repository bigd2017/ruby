require_relative("../db/sql_runner")
require_relative("star")
require_relative("casting")

class Movie

  attr_reader :id
  attr_accessor :name, :genre, :budget

  def initialize( options )
    @id = options['id'].to_i if options['id']
    @title = options['title']
    @genre = options['genre']
    @rating = options['rating']
  end

  def save()
    sql = "INSERT INTO movies
    (  title, genre, rating)
    VALUES
    (  $1,$2, $3)
    RETURNING id"
    values = [@title, @genre, @rating]
    movie = SqlRunner.run( sql, values ).first
    @id = movie['id'].to_i
  end

  def update()
      sql = "UPDATE movies SET (title, genre, rating) = ($1, $2, $3) WHERE id = $4"
      values = [@title, @genre, @rating, @id]
      SqlRunner.run(sql, values)
    end

    def delete()
      sql = "DELETE * FROM movies where id = $1"
      values = [@id]
      SqlRunner.run(sql, values)
    end

    def stars()
      sql = "SELECT stars.* FROM stars INNER JOIN castings ON stars.id = castings.star_id WHERE movie_id = $1"
      values = [@id]
      star_data = SqlRunner.run(sql, values)
      return Star.map_items(star_data)
    end

    def castings()
       sql = "SELECT * FROM castings where movie_id = $1"
       values = [@id]
       casting_data = SqlRunner.run(sql, values)
       return casting_data.map{|casting| Casting.new(casting)}
     end
    # display all the stars for a particular movie
  def self.all()
    sql = "SELECT * FROM movies"
    movie_data = SqlRunner.run(sql)
    return Movie.map_items(mo_data)
  end


  def self.delete_all()
    sql = "DELETE FROM movies"
    SqlRunner.run(sql)
  end

  def self.map_items(movie_data)
    result = movie_data.map { |movie| Movie.new( movie ) }
    return result
  end

end
