-- select distinct count(victims.*), zombies.name, bitings.infected_on
-- from victims inner join bitings on
-- (bitings.victim_id = victims.id)
-- inner join zombies on
-- (bitings.zombie_id = zombies.id)
-- group by zombies.name, bitings.infected_on;


select distinct victims.*, zombies.name, bitings.infected_on
from victims inner join bitings on
(bitings.victim_id = victims.id)
inner join zombies on
(bitings.zombie_id = zombies.id);
