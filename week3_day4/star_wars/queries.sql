-- SELECT characters.*, lightsabers.hilt_metal from characters
-- inner join lightsabers on
-- (characters.id = lightsabers.character_id)
-- order by character_id;

-- This is the simplest, most understood Join and is the most common. This query will return all of the records in the left table (table A)
-- that have a matching record in the right table (table B).

-- select characters.* from characters
-- left join lightsabers
-- on characters.id = lightsabers.character_id;

-- This query will return all of the records in the left table (table A) regardless if any of those records have a match in the right table (table B).
-- It will also return any matching records from the right table.

-- select characters.* ,lightsabers.* from characters
-- right join lightsabers
-- on characters.id = lightsabers.character_id;

-- This query will return all of the records in the right table (table B) regardless if any of those records have a match in the left table (table A).
-- It will also return any matching records from the left table

-- select characters.name, lightsabers.colour from characters
-- inner join lightsabers on
-- (characters.id = lightsabers.character_id);
